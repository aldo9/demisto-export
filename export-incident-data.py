#!/usr/bin/env python

import ast
import argparse
import datetime
import pprint
import json
import os
import ssl
import sys
import urllib.request
import demisto_client
from demisto_client.demisto_api.rest import ApiException


def fail(message):
    print(message)
    sys.exit(1)

def options_handler():
    '''
    Define the options for the command line
    '''
    parser = argparse.ArgumentParser(description='Utility to export incident data from Demisto')
    parser.add_argument('-t', '--token', help='The API token to connect to the Demisto server', required=True)
    parser.add_argument('-s', '--server', help='The server URL to connect to', required=True)
    parser.add_argument('-f', '--folder', help='Optional folder path to where the export will be dumped in sub folders', required=False, default='.')
    parser.add_argument('-q', '--query', help='Optional query in the Demisto query language to filter specific incidents to export', required=False)
    parser.add_argument('-d', '--debug', help='Optional flag print debug info', required=False, type=bool)
    parser.add_argument('--no_verify_ssl', help='Optional flag to not verify SSL certs.', required=False)
    parser.add_argument('--from_date', help='Optional start date for query', required=False, default='2016/06/01')
    parser.add_argument('--to_date', help='Optional end date for query', required=False)
    parser.add_argument('--page_size', help='Optional page size for batches of incidents', required=False, default=1000)
    options = parser.parse_args()

    return options


def parse_date(d):
    '''
    Parse the given datetime using simple format just for dates %Y/%m/%d
    Returns the datetime object or None
    '''
    if d is not None:
        return datetime.datetime.strptime(d, '%Y/%m/%d')
    return None


def main():
    options = options_handler()
    if options.folder is not None and not os.path.exists(options.folder):
        fail('Folder is specified but does not exist')

    verify_ssl = True
    ssl_context = ssl.create_default_context()
    if options.no_verify_ssl:
        ssl_context.check_hostname = False
        ssl_context.verify_mode = ssl.CERT_NONE
        verify_ssl = False
    # Create the api instance
    api_instance = demisto_client.configure(base_url=options.server, api_key=options.token, verify_ssl=verify_ssl, debug=options.debug)

    # Create incident filter object
    filter = demisto_client.demisto_api.SearchIncidentsData()
    # from_date=parse_date(options.from_date), to_date=parse_date(options.to_date)
    inc_filter = demisto_client.demisto_api.IncidentFilter(query=options.query,
        sort=[demisto_client.demisto_api.Order(asc=True, field='id')], size=options.page_size)
    filter.filter = inc_filter
    page = 0
    done = False
    try:
        # Search incidents by filter
        while not done:
            filter.filter.page = page
            api_response = api_instance.search_incidents(filter=filter)
            for incident in api_response['data']:
                print('Handling incident %s' % incident['id'])
                incident_path = options.folder + '/' + incident['id']
                if not os.path.exists(incident_path):
                    os.makedirs(incident_path)
                with open(incident_path + '/incident.json', 'w') as f:
                    f.write(json.dumps(incident, sort_keys=True, indent=4, separators=(',', ': ')))
                # Now load investigation, child investigations and entries
                try:
                    inv_response = api_instance.generic_request(path='/investigation/' + incident['id'], method='POST', body={'pageSize': 10000})
                    inv_data = ast.literal_eval(inv_response[0])
                    with open(incident_path + '/investigation.json', 'w') as f:
                        f.write(json.dumps(inv_data, sort_keys=True, indent=4, separators=(',', ': ')))
                    for entry in inv_data['entries']:
                        if entry['file']:
                            file_req = urllib.request.Request(options.server + '/entry/download/%s' % entry['id'])
                            file_req.add_header('Authorization', options.token)
                            with open(incident_path + '/' + entry['file'], 'wb') as file_output:
                                with urllib.request.urlopen(file_req, context=ssl_context) as file_data:
                                    file_output.write(file_data.read())

                except ApiException as investigations_e:
                    print("Exception when loading investigation for incident %s: %s\n" % (incident['id'], investigations_e))

            page = page + 1
            done = len(api_response['data']) < options.page_size
        print('Total number of incidents exported: %d\n' % api_response['total'])
    except ApiException as e:
        print("Exception when calling incidents search: %s\n" % e)


if __name__ == '__main__':
    main()
